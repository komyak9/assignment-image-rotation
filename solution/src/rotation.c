#include "rotation.h"

struct image rotate(struct image const* source) {
  const uint64_t size = source->height * source->width;
  struct pixel* rotated_pixels = malloc(sizeof(struct pixel) * size);
  const struct pixel* original_pixels = source->data;

  struct image result = image_create(source->height, source->width);
  for(size_t i = 0; i < source->height; i = i + 1) {
    for(size_t j = 0; j < source->width; j = j + 1) {
    rotated_pixels[source->height * j + (source->height - i - 1)] = original_pixels[i * source->width + j];
    }
  }
  
  result.data = rotated_pixels;
  return result;
}
