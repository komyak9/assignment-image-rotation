#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include "image.h"
#include "status.h"

enum status_read from_bmp( FILE* in, struct image* img );
enum status_write to_bmp( FILE* out, struct image const* img );
