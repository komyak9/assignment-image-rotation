#ifndef STATUS
#define STATUS

enum status_read  {
  READ_OK = 0,
  READ_FILE_OK, 
  READ_HEADER_ERR,
  READ_FILE_ERR
};

enum status_write  {
  WRITE_OK = 0,
  WRITE_FILE_OK,
  WRITE_ERR,
  WRITE_FILE_ERR
};

#endif
