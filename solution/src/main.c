#include "bmp.h"
#include "file_actions.h"
#include "image.h"
#include "rotation.h"
#include <stdio.h>

static const char* const rd_status[] = {
  [READ_OK] = "Данные успешно считаны.\n",
  [READ_HEADER_ERR] = "ОШИБКА: не удалось прочитать header.\n",
  [READ_FILE_OK] = "Файл успешно считан.\n",
  [READ_FILE_ERR] = "ОШИБКА: не удалось прочесть файл.\n"
};  

static const char* const wr_status[] = {
  [WRITE_OK] = "Данные успешно записаны.\n",
  [WRITE_ERR] = "ОШИБКА: данные не были записаны.\n",
  [WRITE_FILE_ERR] = "ОШИБКА: данные в файл не были записаны.\n",
  [WRITE_FILE_OK] = "Данные успешно записаны в файл.\n"
};

void write_to_stdout(const char* status) {
  fprintf(stdout, "%s", status);
}

void write_to_stderr(const char* status) {
  fprintf(stderr, "%s", status);
}

int main( int argc, char** argv ) {
    (void) argc; (void) argv; // supress 'unused parameters' warning

    if (argc != 3) {
      fprintf(stderr, "%s", "Некорректный ввод аргументов. Проверьте ввод. Программа завершается.");
      return 0;
    }
    struct image source = {0};
    struct image result = {0};
    const enum status_read r_status = image_read(argv[1], &source);
    
    if (r_status > 1) {
      write_to_stdout(rd_status[r_status]);
      return 0;
    } else write_to_stdout(rd_status[r_status]);

    result = rotate(&source);
    const enum status_write w_status = image_write(argv[2], &result);
    if (w_status > 1) {
      write_to_stderr(wr_status[w_status]);
      return 0;
    } else write_to_stderr(wr_status[w_status]);

    image_delete(result);
    image_delete(source);  
    return 0;
}
