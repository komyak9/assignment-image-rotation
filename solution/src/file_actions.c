#include "file_actions.h"

enum status_read image_read(char* const filename, struct image* const img) {
  FILE *fPtr;
  fPtr = fopen(filename,"rb");
  if (!fPtr) return READ_FILE_ERR;

  const enum status_read r_status = from_bmp(fPtr, img);
  if (r_status) return r_status;

  fclose(fPtr);
  return READ_FILE_OK;
}

enum status_write image_write(char* const filename, struct image* const img){
  FILE *fPtr;
  fPtr = fopen(filename,"wb");
  if (!fPtr) return WRITE_FILE_ERR;

  const enum status_write w_status = to_bmp(fPtr, img);
  if (w_status) return w_status;

  fclose(fPtr);
  return WRITE_FILE_OK;
}
