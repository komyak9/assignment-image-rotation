#ifndef FILE_OPS
#define FILE_OPS
#include <stdio.h>
#include "image.h"
#include "status.h"
#include "bmp.h"
enum status_read image_read(char* const filename, struct image* const img);
enum status_write image_write(char* const filename, struct image* const img);
#endif
